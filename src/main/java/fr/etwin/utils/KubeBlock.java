package fr.etwin.utils;

import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;

// Wood blocks use Log
// Leaves blocks use leaves
// Biomes blocks use terracotta/concrete
// Transmutable and special blocks use glass
// No real significations in colors
public enum KubeBlock {
    Empty           (0, Material.AIR),
    Fixed           (1, Material.LIGHT_GRAY_CONCRETE),    // Béton
    Water           (2, Material.WATER),    // Eau

    SoilTree        (3, Material.TERRACOTTA),    // Terre Brune
    SoilTree1       (4, Material.BIRCH_LOG),    // Bois Bouleau
    SoilTree2       (5, Material.BIRCH_LEAVES),    // Feuilles Bouleau

    AutumnTree      (6, Material.ORANGE_TERRACOTTA),    // Terre d'Automne
    AutumnTree1     (7, Material.ACACIA_LOG),    // Bois d'Automne
    AutumnTree2     (8, Material.ACACIA_LEAVES),    // Feuilles d'Automne

    HighTree        (9, Material.BLACK_TERRACOTTA),    // Terre Sombre
    HighTree1       (10, Material.DARK_OAK_LOG),   // Bois Sombre
    HighTree2       (11, Material.BLACK_CONCRETE),   // Feuilles Sombres
    HighTree3       (12, Material.DARK_OAK_LEAVES),   // Herbes Sombres

    Clouds          (13, Material.WHITE_CONCRETE),   // Terre Bleue
    Clouds1         (14, Material.WHITE_GLAZED_TERRACOTTA),   // Coeur de Nuage
    Clouds2         (15, Material.WHITE_TERRACOTTA),   // Nuage

    SphereTree      (16, Material.OAK_LOG),   // Bois Clair
    SphereTree1     (17, Material.OAK_LEAVES),   // Feuilles Roses

    LargeTree       (18, Material.ORANGE_GLAZED_TERRACOTTA),   // Racines
    LargeTree1      (19, Material.STRIPPED_ACACIA_LOG),   // Bois Géant
    LargeTree2      (20, Material.AZALEA_LEAVES),   // Feuilles Géantes

    SnowSapin       (21, Material.YELLOW_TERRACOTTA),   // Terre-Neige
    SnowSapin1      (22, Material.SPRUCE_LOG),   // Bois Sapin
    SnowSapin2      (23, Material.SPRUCE_LEAVES),   // Feuilles-Neige

    Jungle          (24, Material.BLUE_TERRACOTTA),   // Jungle
    Jungle1         (25, Material.BLUE_CONCRETE),   // Lianes Sombres
    Jungle2         (26, Material.LIGHT_BLUE_TERRACOTTA),   // Lianes Claires
    Jungle3         (27, Material.LIGHT_BLUE_CONCRETE),   // Lianes Pourries

    Caverns         (28, Material.MAGENTA_CONCRETE),   // Mur
    Caverns1        (29, Material.MAGENTA_TERRACOTTA),   // Roc
    Caverns2        (30, Material.MAGENTA_GLAZED_TERRACOTTA),   // Pilier

    Field           (31, Material.GREEN_CONCRETE),   // Prairie
    Field1          (32, Material.GREEN_GLAZED_TERRACOTTA),   // Herbes Claires
    Field2          (33, Material.GREEN_TERRACOTTA),   // Herbes

    Swamp           (34, Material.CYAN_TERRACOTTA),   // Marais
    Swamp1          (35, Material.JUNGLE_LOG),   // Bois Marrais
    Swamp2          (36, Material.JUNGLE_LEAVES),   // Feuilles Marais
    Swamp3          (37, Material.CYAN_GLAZED_TERRACOTTA),   // Boue

    SoilPeaks       (38, Material.LIME_TERRACOTTA),   // Roche Froide
    SoilPeaks1      (39, Material.LIME_GLAZED_TERRACOTTA),   // Roche Volcanique

    PilarPlain      (40, Material.PINK_TERRACOTTA),   // Cendres
    PilarPlain1     (41, Material.PINK_GLAZED_TERRACOTTA),   // Grès
    PilarPlain2     (42, Material.PINK_CONCRETE),   // Totem Antique

    FlowerPlain     (43, Material.GRAY_CONCRETE),   // Pâturages
    FlowerPlain1    (44, Material.GRAY_TERRACOTTA),   // Fleur Blanche
    FlowerPlain2    (45, Material.GRAY_GLAZED_TERRACOTTA),   // Fleur Rose
    FlowerPlain3    (46, Material.LIGHT_GRAY_CONCRETE),   // Stonehenge

    Savana          (47, Material.ORANGE_CONCRETE),   // Savane
    Savana1         (48, Material.RED_CONCRETE),   // Arbuste Sec
    Savana2         (49, Material.RED_TERRACOTTA),   // Zèbre
    Savana3         (50, Material.RED_GLAZED_TERRACOTTA),   // Eléphant

    Desert          (51, Material.YELLOW_CONCRETE),   // Sable
    Desert1         (52, Material.YELLOW_GLAZED_TERRACOTTA),   // Cactus

    Fruit           (53, Material.BROWN_TERRACOTTA),   // Gland
    Lava            (54, Material.LAVA),   // Lave
    Dolpart         (55, Material.BLACK_STAINED_GLASS),   // Sombre
    Dolmen          (56, Material.BEDROCK),   // Dolmen

    Flying          (57, Material.PURPLE_CONCRETE),   // Méka
    Flying1         (58, Material.PURPLE_TERRACOTTA),   // Métal
    Flying2         (59, Material.PURPLE_GLAZED_TERRACOTTA),   // Acier

    Koala           (60, Material.BROWN_GLAZED_TERRACOTTA),   // Koala

    Invisible       (61, Material.GLASS),   // Invisible
    Amethyste       (62, Material.PURPLE_STAINED_GLASS),   // Améthyste
    Emeraude        (63, Material.GREEN_STAINED_GLASS),   // Emeraude
    Rubis           (64, Material.RED_STAINED_GLASS),   // Rubis
    Saphir          (65, Material.BLUE_STAINED_GLASS),   // Saphir
    Fog             (66, Material.GRAY_STAINED_GLASS),   // Brouillard
    Shade           (67, Material.LIGHT_GRAY_STAINED_GLASS),   // Ombre
    Light           (68, Material.YELLOW_STAINED_GLASS),   // Lumière
    Gold            (69, Material.GOLD_BLOCK),   // Or
    Message         (70, Material.ORANGE_STAINED_GLASS),   // Forum
    Teleport        (71, Material.WHITE_STAINED_GLASS),   // Téléport
    Jump            (72, Material.RED_STAINED_GLASS),   // Ressort
    Chest           (73, Material.BROWN_STAINED_GLASS),   // Kofre
    Multicol        (74, Material.PURPLE_STAINED_GLASS),   // Rubix
    MTwin           (75, Material.PINK_STAINED_GLASS),   // Motion Twin
    Crate           (76, Material.LIME_STAINED_GLASS),   // Caisse
    Noel            (77, Material.LIGHT_BLUE_STAINED_GLASS);   // Noël

    public final int id;
    public final Material minecraftMaterial;

    KubeBlock(int id, Material minecraftMaterial) {
        this.id = id;
        this.minecraftMaterial = minecraftMaterial;
    }

    private static final Map<Integer, KubeBlock> map = new HashMap<>();
    public static KubeBlock from(int i){
        return map.get(i);
    }
    static {
        for (KubeBlock block: KubeBlock.values()) {
            map.put(block.id, block);
        }
    }
}
